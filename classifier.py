import pickle
import utils
import numpy as np
import pandas as pd 
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from pathlib import Path


tokenizer = pickle.load(Path('./tokenizer.pickle').open('rb'))
model = load_model('./trained.nlp.h5')

def is_real(text: str) -> bool:
    """
    Return is real news
    """
    seq = tokenizer.texts_to_sequences([text])
    pad_seq = pad_sequences(seq, maxlen=300)
    prediction = model.predict(pad_seq)
    return bool(utils.ceil(prediction[0]))

