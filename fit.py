import utils
import numpy as np
import pandas as pd 
import seaborn as sns
from pathlib import Path
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras import models, layers


ds_path = Path('/media/aenelle/CCCOMA_X64F/572515_1037534_bundle_archive/final.csv')
glove_path = Path('/home/aenelle/Desktop/glove.840B.300d.txt')
df = pd.read_csv(ds_path)

def build_tokenizer(texts):
    token = Tokenizer()
    token.fit_on_texts(texts)
    return token

def build_model(matrix, emb_vocab_size):
	model = models.Sequential()
	model.add(layers.Embedding(emb_vocab_size,300,weights = [matrix],input_length=300,trainable = False))
	model.add(layers.Bidirectional(layers.LSTM(75)))
	model.add(layers.Dense(32,activation = 'relu'))
	model.add(layers.Dense(1,activation = 'sigmoid'))
	model.compile(optimizer='adam',loss='binary_crossentropy',metrics = ['accuracy'])
	return model

def make_matrix(emb_vocab_size, glove_f, tokenizer):
    embedding_vector = {}
    for line in glove_f:
        value = line.split(' ')
        word = value[0]
        coef = np.array(value[1:],dtype = 'float32')
        embedding_vector[word] = coef

    embedding_matrix = np.zeros((emb_vocab_size,300))
    for word,i in tokenizer.word_index.items():
        embedding_value = embedding_vector.get(word)
        if embedding_value is not None:
            embedding_matrix[i] = embedding_value
    
    return embedding_matrix

def build_and_fit_model(dframe, tokenizer, glove, epochs=10, batch_size=256, validation_split=0.2):
    shuffled_df = dframe.sample(frac=1).reset_index(drop=True)
    split = (len(dframe) // 3) * 2
    train_df = shuffled_df[:split]
    test_df = shuffled_df[split:]
    seq = tokenizer.texts_to_sequences(train_df['text'])
    pad_seq = pad_sequences(seq,maxlen=300)
    vocab_size = len(tokenizer.word_index)+1
    matrix = make_matrix(vocab_size, glove, tokenizer)
    model = build_model(matrix, vocab_size)
    history = model.fit(pad_seq, train_df['is_real'], epochs=epochs, batch_size=batch_size, validation_split=validation_split)
    x_test = tokenizer.texts_to_sequences(test_df['text'])
    test_seq = pad_sequences(x_test, maxlen=300)
    prediction = model.predict(test_seq)
    for i in range(len(test_df)):
        is_real = test_df[i:i+1]['is_real']
        assert utils.ceil(prediction[i]) == is_real
    return model, history

# model = build_model()
#model = models.load_model('./nofit.nlp.model')
# train_df = 

