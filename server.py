import flask
import classifier


app = flask.Flask(__name__, static_url_path='', static_folder='front')

@app.route('/check', methods=['POST'])
def check():
    text = flask.request.json['text']
    return flask.jsonify(status='ok', isReal=classifier.is_real(text))

@app.route('/', methods=['GET'])
def index():
    return flask.current_app.send_static_file('index.html')

