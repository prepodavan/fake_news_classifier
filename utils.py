def ceil(fl: float, beginning: int = 0, end: int = 1) -> int:
    mid = (end - beginning) / 2
    return beginning if fl < mid else end

